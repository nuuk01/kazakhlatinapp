import React, {Component} from 'react';
import kazakhAlphabetMap from './KazakhAlphabet';
import TranslatedWord from './TranslatedWord';
import  WordsTable from './WordsTable';
class Translator extends Component{
    constructor(props){
        super(props)
        this.state = {
            lastWord : '',
            words : [] 
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleClick  = this.handleClick.bind(this)
        this.handleTranslate = this.handleTranslate.bind(this)
    }

    handleChange(event){
        const {name, value} = event.target;
        this.setState({
            [name] : value
        });
        //console.log('Last Word:' + this.state.lastWord);
    }
    handleTranslate(word){
        var temp = '';
        let prevWords = this.state.words;
        for(let i=0; i < word.length; ++i){
            if(kazakhAlphabetMap.has(word[i])){
                temp = temp + kazakhAlphabetMap.get(word[i]);
            }else{
                temp = temp + word[i];
            }
        }
        if (word !==''){
            prevWords.push([word,temp]);
            //console.log(temp);
            this.setState({
                lastWord : '',
                words : prevWords
            });
            //this.state.words.map(([val1,vall2]) => {console.log(val1 + ":" +vall2)});
        }
    }
    handleClick(event){
        console.log(this.state.lastWord);
        this.handleTranslate(this.state.lastWord);
        event.preventDefault();
        console.log('Click worked');
    }
    render(){
        const translatedWords = this.state.words.slice(0).reverse().map( ([val1,val2]) => <TranslatedWord key={val1} name1={val1} name2={val2}/>);
        var checkForNullWords;
        if (this.state.words.length){
            checkForNullWords = <WordsTable list={translatedWords}/>
        }
        return(
            <div>
            <form onClick={this.handleClick}>
                <input type="text" name="lastWord" value={this.state.lastWord} onChange={this.handleChange} placeholder="Напишите слово..."/>
                <input type="submit" value="Переведи мне"/>
            </form>
            <br/>
            {checkForNullWords} 
            </div>

        );
    }
}
;
export default Translator;
