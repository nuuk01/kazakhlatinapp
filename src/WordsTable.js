import React from 'react';

function WordsTable(props){
    return(
        <div>
            <table>
                <tbody>
                <tr>
                    <th>Переведенные слова</th>
                </tr>
                {props.list}
                </tbody>
            </table>
        </div>   
    );
}
;
export default WordsTable;