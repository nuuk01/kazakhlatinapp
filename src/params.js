const particlesParam ={
	    particles: {
	        number: {
	            value: 25
	        },
	        size: {
	            value: 4
	        }
	    }
};
export default particlesParam;