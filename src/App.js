import React from 'react';
import './App.css';
import Header from './Header';
import Translator from './Translator';
import Particles from 'react-particles-js';
import particlesParam from './params';
function App() {
  return (
	  		<div className ="background">
	  		<Header />
			<Translator />
			</div>
			);
}

export default App;
